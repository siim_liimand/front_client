```js
import { addSiteRoute } from 'client/config/routes/site-routes';
import { IGetPage } from 'client/router/interfaces';
import { addComponent } from 'client/router';

// constants/pages
const PAGE_HOME = 'h';

// constants/sites
const SITE_DEFAULT = 'd';

addComponent(PAGE_HOME, (getPage: IGetPage): void => {
  import(/* webpackChunkName: "home" */ '@pages/home').then((module) => {
    getPage(module.default);
  });
});

addSiteRoute(SITE_DEFAULT, '127.0.0.1');

createClient();
```
