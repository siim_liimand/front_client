const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = {
  name: 'client',
  entry: {
    client: path.resolve(__dirname, 'src/client.tsx'),
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname + '/dist/static'),
    filename: '[name].[contenthash].js',
    publicPath: '',
    uniqueName: 'fd',
    chunkLoadingGlobal: 'wpfd',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@app': path.resolve(__dirname, 'src/app'),
      '@actions': path.resolve(__dirname, 'src/actions'),
      '@router': path.resolve(__dirname, 'src/router'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@constants': path.resolve(__dirname, 'src/constants'),
      '@routes': path.resolve(__dirname, 'src/routes'),
      '@services': path.resolve(__dirname, 'src/services'),
      '@config': path.resolve(__dirname, 'src/config'),
      '@pages': path.resolve(__dirname, 'src/pages'),
      '@hooks': path.resolve(__dirname, 'src/hooks'),
      '@context': path.resolve(__dirname, 'src/context-providers'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      '@public': path.resolve(__dirname, 'src/public'),
      '@lib': path.resolve(__dirname, 'src/lib'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: 'tsconfig.json',
          transpileOnly: true,
        },
      },
      {
        test: /\/images\/icons\/[a-z0-9_-]+\.(png|jpe?g|svg|ico|webp)$/i,
        loader: 'file-loader',
        options: {
          name: '/images/icons/[name].[ext]',
        },
      },
      {
        test: /\/images\/screenshots\/[a-z0-9_-]+\.(png|jpe?g|svg|ico|webp)$/i,
        loader: 'file-loader',
        options: {
          name: '/images/screenshots/[name].[ext]',
        },
      },
    ],
  },
  target: 'web',
  plugins: [
    new CleanWebpackPlugin(),
    new WebpackManifestPlugin(),
    // new BundleAnalyzerPlugin(),
    new ForkTsCheckerWebpackPlugin(),
    new WorkboxPlugin.InjectManifest({
      swSrc: './src/public/sw.js',
      swDest: 'sw.js',
      exclude: [/\.map$/, /manifest$/, /\.htaccess$/, /service-worker\.js$/, /sw\.js$/],
    }),
  ],
};
