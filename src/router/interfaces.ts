import { ReactFC } from '@lib/xreact';
import { IPageProps } from '@pages/interfaces';
import { IHtmlPageData } from '@services/get-page-data/interface';

export type IGetPage = (Component: ReactFC<IPageProps>) => void;

export type IRouteComponent = (getPage: IGetPage) => void;

export interface IRouteComponents {
  [routeName: string]: IRouteComponent;
}

export type IResolveRoute = (getComponent: IRouteComponent, pageData: IHtmlPageData) => void;

export type IRejectRoute = (err: Error) => void;
