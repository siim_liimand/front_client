import { createError } from '@actions/error';
import { ROUTE_TYPE_COMPONENT } from '@config/routes/interface';
import { ERROR_IVALID_ROUTE_TYPE, ERROR_ROUTE_NOT_FOUND } from '@constants/erors';
import { getPageData } from '@services/get-page-data/index';
import { IHtmlPageData, IPageData } from '@services/get-page-data/interface';

import { IRejectRoute, IResolveRoute, IRouteComponent, IRouteComponents } from './interfaces';

const routeComponents: IRouteComponents = {};

/**
 * Add component
 * @param {string} routeName
 * @param {IRouteComponent} routeComponent
 */
export function addComponent(routeName: string, routeComponent: IRouteComponent): void {
  routeComponents[routeName] = routeComponent;
}

/**
 * Get route
 * @param {string} hostName
 * @param {string} path
 * @param {URLSearchParams} searchParams
 * @param {IResolveRoute} resolve
 * @param {IRejectRoute} reject
 */
export function getRoute(
  hostName: string,
  path: string,
  searchParams: URLSearchParams,
  resolve: IResolveRoute,
  reject: IRejectRoute,
): void {
  getPageData(hostName, path, searchParams, resolveGetPageData(resolve, reject), reject);
}

type IResolveGetPageData = (pageData: IPageData) => void;

/**
 * Resolve get page data
 * @param {IResolveRoute} resolve
 * @param {IRejectRoute} reject
 * @returns {(pageData: IPageData) => void}
 */
const resolveGetPageData =
  (resolve: IResolveRoute, reject: IRejectRoute): IResolveGetPageData =>
  (pageData: IPageData): void => {
    const type = pageData.type;
    const routeName = pageData.routeKey;

    if (type === ROUTE_TYPE_COMPONENT) {
      const routeComponent = getRouteComponent(routeName);
      if (routeComponent) {
        resolve(routeComponent, getHtmlPageData(pageData));
      } else {
        reject(createError(ERROR_ROUTE_NOT_FOUND));
      }
    } else {
      reject(createError(ERROR_IVALID_ROUTE_TYPE));
    }
  };

/**
 * Get html page data
 * @param {IPageData} pageData
 * @returns {IHtmlPageData}
 */
function getHtmlPageData(pageData: IPageData): IHtmlPageData {
  return pageData as IHtmlPageData;
}

/**
 * Get route component
 * @param {string} routeName
 * @returns {IRouteComponent | undefined}
 */
function getRouteComponent(routeName: string): IRouteComponent | undefined {
  return routeComponents[routeName] || undefined;
}
