export const CONST_UNDEFINED = 'undefined';
export const ERROR_ROUTE_NOT_FOUND = '1';
export const ERROR_IVALID_ROUTE_TYPE = '2';
export const ERROR_INVALID_SITE = '3';
export const ERROR_PAGE_NOT_FOUND = '4';
export const ERROR_SITE_NOT_DEFINED = '5';
