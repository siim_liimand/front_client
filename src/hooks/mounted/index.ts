import { useEffect, useState } from '@lib/xreact';

export const useMounted = () => {
    const [mounted, setMounted] = useState<boolean>(false);
    useEffect(() => {
        setMounted(true);

        return () => {
            setMounted(false);
        };
    }, []);

    return mounted;
};