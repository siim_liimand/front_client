import { ReactFCE } from '@lib/xreact';
import { JSX } from 'preact/jsx-runtime';

export interface IDynamicImportComponentProps {
  children?: JSX.Element | null;
}

export type IDynamicImportComponent = ReactFCE<IDynamicImportComponentProps>;

export interface IDynamicImportComponents {
  [key: string]: IDynamicImportComponent;
}

export type IResolveDynamicImportCallback = (value: IDynamicImportComponent) => void;

export type IDynamicImportCallback = (resolve: IResolveDynamicImportCallback) => void;

export interface IDynamicImportCallbackData {
  key: string;
  callback: IDynamicImportCallback;
}

export type IDynamicImportResponse = () => IDynamicImportComponent | null;
