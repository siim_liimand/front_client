import { useMainContext } from '@hooks/main-context';
import { useState } from '@lib/xreact';

import {
  IDynamicImportCallback,
  IDynamicImportComponent,
  IDynamicImportResponse,
} from './interfaces';

/**
 * Use dynamic import
 * @param {string} key
 * @param {IDynamicImportCallback} callback
 * @returns {IDynamicImportResponse}
 */
export function useDynamicImport(
  key: string,
  callback: IDynamicImportCallback,
): IDynamicImportResponse {
  return function DynamicImport(): IDynamicImportComponent | null {
    const [addImportCallback, getImportedComponent, setImportedComponent] = useMainContext();
    const [component, setComponent] = useState(getImportedComponent(key));

    if (getImportedComponent(key) === null) {
      addImportCallback(key, callback, (comp: IDynamicImportComponent) => {
        setImportedComponent(key, comp);
        setComponent(comp);
      });
    }

    return component;
  };
}
