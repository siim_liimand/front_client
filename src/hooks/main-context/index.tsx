import { MainContext } from '@context/main-context-provider/index';
import { IMainContext } from '@context/main-context-provider/interfaces';
import { useContext } from '@lib/xreact';

/**
 * Use main context
 * @returns {ImainContext}
 */
export function useMainContext(): IMainContext {
  return useContext(MainContext);
}
