import { useState } from '@lib/xreact';

type ICallback = (value: any) => void;
type IListener = [string, string, ICallback];
type IListeners = IListener[];

type IListenStateChange = (key: string) => any;
type IChangeState = (key: string, value: any) => void;
type IRemoveListener = () => void;
export type IGlobalState = [IListenStateChange, IChangeState, IRemoveListener];
export type IGetGlobalState = () => IGlobalState;

export const LISTEN_STATE_CHANGE = 0;
export const CHANGE_STATE = 1;
export const REMOVE_LISTENER = 2;

const listeners: IListeners = [];

export const getGlobalState = (): IGetGlobalState => {
  const id = '' + Math.random().toString(36).substr(2, 9);
  
  return ((): IGlobalState => {
    const [state, setState] = useState<any>(null);

    const listenStateChange: IListenStateChange = (key: string): any => {
      removeListener();
      const listener: IListener = [id, key, setState];
      listeners.push(listener);
  
      return state;
    };
  
    const changeState = (key: string, value: any): void => {
      for (let i = 0, count = listeners.length; i < count; i += 1) {
        const [n, k, c] = listeners[i];
        if (!!n && k === key) {
          c(value);
        }
      }
    };
  
    const removeListener: IRemoveListener = (): void => {
      const index = listeners.findIndex(([n]) => n === id);
      if (index !== -1) {
        listeners.splice(index, 1);
      }
    };
  
    return [listenStateChange, changeState, removeListener];
  });
};
