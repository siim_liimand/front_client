import { useDynamicImport } from '@hooks/dynamic-import';
import {
  IDynamicImportCallback,
  IResolveDynamicImportCallback,
} from '@hooks/dynamic-import/interfaces';
import { Fragment, ReactFC, ReactFCE } from '@lib/xreact';
import { IPageProps } from '@pages/interfaces';
import { getRoute } from '@router/index';
import { IRejectRoute, IResolveRoute, IRouteComponent } from '@router/interfaces';
import { IHtmlPageData } from '@services/get-page-data/interface';

import { IAppProps } from './interfaces';

type IResolveRouteProps = (resolve: IResolveDynamicImportCallback) => IResolveRoute | IRejectRoute;

/**
 * App
 * @param {IAppProps} props
 * @returns {ReactFCE<IPageProps>}
 */
export const App: ReactFC<IAppProps> = ({ h, q, p }: IAppProps): ReactFCE<IPageProps> => {
  /**
   * Get component
   * @param resolve
   */
  const getComponent: IDynamicImportCallback = (resolve: IResolveDynamicImportCallback): void => {
    getRoute(h, p, q, resolveRoute(resolve) as IResolveRoute, rejectRoute(resolve) as IRejectRoute);
  };

  /**
   * Resolve route
   * @param resolve
   * @returns {IResolveRoute}
   */
  const resolveRoute: IResolveRouteProps =
    (resolve: IResolveDynamicImportCallback): IResolveRoute =>
    (getRouteComponent: IRouteComponent, pageData: IHtmlPageData): void => {
      getRouteComponent((RouteComponent: ReactFC<IPageProps>): void => {
        resolve(<RouteComponent pageData={pageData} />);
      });
    };

  /**
   * Reject route
   * @param resolve
   * @returns {IRejectRoute}
   */
  const rejectRoute: IResolveRouteProps =
    (resolve: IResolveDynamicImportCallback): IRejectRoute =>
    (error: Error): void => {
      resolve(<ErrorComponent error={error} />);
    };

  const Component = useDynamicImport('p', getComponent);

  return <Component />;
};

interface IErrorComponentProps {
  error: Error;
}

/**
 * Error component
 * @param {IErrorComponentProps} props
 * @returns {ReactFCE<any>}
 */
function ErrorComponent(props: IErrorComponentProps): ReactFCE<null> {
  return <Fragment>{props.error.message}</Fragment>;
}
