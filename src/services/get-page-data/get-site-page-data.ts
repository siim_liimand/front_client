import { getDefaultSiteRoute, getSiteRoute } from '@config/routes/site-routes';
import { ISitePageData } from './interface';

function findSite(hostName: string): ISitePageData | undefined {
  return getSiteRoute(hostName);
}

export function isInSiteRoutes(hostName: string): boolean {
  const routeItemContent: ISitePageData | undefined = findSite(hostName);
  return routeItemContent !== undefined;
}

export function getSitePageData(hostName: string): ISitePageData {
  return findSite(hostName) || getDefaultSiteRoute();
}
