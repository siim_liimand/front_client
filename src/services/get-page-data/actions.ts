import { ROUTE_TYPE_ERROR } from '@config/routes/interface';
import { getSitePageData, isInSiteRoutes } from './get-site-page-data';
import { IPageData, ISitePageData } from './interface';

export function getErrorPageData(e: Error): IPageData {
  return {
    message: e.message,
    routeKey: ROUTE_TYPE_ERROR,
    type: ROUTE_TYPE_ERROR,
  };
}

export function getSite(hostName: string): ISitePageData | undefined {
  let site: ISitePageData | undefined;
  if (isInSiteRoutes(hostName)) {
    site = getSitePageData(hostName);
  }

  return site;
}
