import { createError } from '@actions/error';
import { ROUTE_TYPE_COMPONENT } from '@config/routes/interface';
import { ERROR_PAGE_NOT_FOUND } from '@constants/erors';
import { getErrorPageData } from './actions';
import { IHtmlPageData, IPageData, ISitePageData } from './interface';

export function getHtmlPageData(
  site: ISitePageData,
  path: string,
  searchParams: URLSearchParams,
  resolve: (pageData: IPageData) => void,
  reject: (err: Error) => void,
): void {
  const [siteName] = site;
  if (path === '/') {
    return resolve(getHomePageData(siteName));
  } else if (path === '/test') {
    return resolve(getTestPageData(siteName));
  }

  return resolve(getErrorPageData(createError(ERROR_PAGE_NOT_FOUND)));
}

function getHomePageData(siteName: string): IHtmlPageData {
  return {
    countryCode: 'EE',
    languageCode: 'et',
    pagePath: [
      {
        name: 'h',
        routeKey: 'h',
        slug: '',
      },
    ],
    routeKey: 'h',
    searchParams: new URLSearchParams(''),
    siteName,
    type: ROUTE_TYPE_COMPONENT,
  };
}

function getTestPageData(siteName: string): IHtmlPageData {
  return {
    countryCode: 'EE',
    languageCode: 'et',
    pagePath: [
      {
        name: 'h',
        routeKey: 'h',
        slug: '',
      },
      {
        name: 't',
        routeKey: 't',
        slug: 't',
      },
    ],
    routeKey: 't',
    searchParams: new URLSearchParams('q=test&itemsPerPage=10'),
    siteName,
    type: ROUTE_TYPE_COMPONENT,
  };
}
