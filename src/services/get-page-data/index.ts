import { createError } from '@actions/error';
import { ERROR_SITE_NOT_DEFINED } from '@constants/erors';
import { getErrorPageData, getSite } from './actions';
import { getHtmlPageData } from './get-html-page-data';
import { IPageData } from './interface';

export function getPageData(
  hostName: string,
  path: string,
  searchParams: URLSearchParams,
  resolve: (pageData: IPageData) => void,
  reject: (err: Error) => void,
): void {
  try {
    const site = getSite(hostName);
    if (site !== undefined) {
      return getHtmlPageData(site, path, searchParams, resolve, reject);
    }
  } catch (e) {
    return resolve(getErrorPageData(e));
  }

  return resolve(getErrorPageData(createError(ERROR_SITE_NOT_DEFINED)));
}
