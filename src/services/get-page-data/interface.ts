import { ALL_ROUTE_TYPES } from '@config/routes/interface';

export interface IHtmlPagePathItem {
  name: string;
  slug: string;
  routeKey: string;
}

interface IGeneralPageData {
  type: ALL_ROUTE_TYPES;
  routeKey: string;
}

export type ISitePageData = [hostName: string, siteName: string];

export interface IHtmlPageData extends IGeneralPageData {
  countryCode: string;
  languageCode: string;
  siteName: string;
  pagePath: IHtmlPagePathItem[];
  searchParams: URLSearchParams;
}

export interface IErrorPageData extends IGeneralPageData {
  message: string;
}

export type IPageData = IHtmlPageData | IErrorPageData;
