import { createError } from '@actions/error';
import { ERROR_INVALID_SITE } from '@constants/erors';
import { ISitePageData } from '@services/get-page-data/interface';

export interface ISiteRoutes {
  [hostname: string]: ISitePageData;
}

const siteRoutes: ISiteRoutes = {};
const siteNames: string[] = [];

export function addSiteRoute(siteName: string, hostname: string): void {
  siteRoutes[hostname] = [hostname, siteName];
  siteNames.push(siteName);
}

/**
 * Get default route
 * @returns {ISitePageData}
 */
export function getDefaultSiteRoute(): ISitePageData {
  const hostNames = Object.keys(siteRoutes);
  const firstHostName: string = hostNames[0] || '';
  const route: ISitePageData | undefined = siteRoutes[firstHostName] || undefined;

  if (route === undefined) {
    throw createError(ERROR_INVALID_SITE);
  }

  return route;
}

/**
 * Get site route
 * @param {string} hostName
 * @returns {ISitePageData | undefined}
 */
export function getSiteRoute(hostName: string): ISitePageData | undefined {
  return siteRoutes[hostName];
}
