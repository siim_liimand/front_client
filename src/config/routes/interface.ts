import { ReactFC } from '@lib/xreact';
import { IPageProps } from '@pages/interfaces';

export const ROUTE_TYPE_COMPONENT = 'c';
export const ROUTE_TYPE_ERROR = 'e';

export type T_ROUTE_TYPE_COMPONENT = 'c';
export type T_ROUTE_TYPE_ERROR = 'e';

export type ALL_ROUTE_TYPES = T_ROUTE_TYPE_COMPONENT | T_ROUTE_TYPE_ERROR;

export type IRouteHtmlContent = ReactFC<IPageProps>;

export type IRouteContent = IRouteHtmlContent;
