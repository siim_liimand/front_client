import { Test1 } from './home/test1';
import { Test2 } from './home/test2';

const Home = () => {
  return (
    <div>
      <Test1 />
      <br />
      <Test2 />
    </div>
  );
};

export default Home;
