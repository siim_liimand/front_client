import {
  CHANGE_STATE,
  IGlobalState,
  IGetGlobalState,
  LISTEN_STATE_CHANGE,
  getGlobalState,
} from '@hooks/global-state';
import { LISTENER_TEST1_CLICK } from './constants';

const useGlobalState: IGetGlobalState = getGlobalState();

export const Test1 = () => {
  const globalState: IGlobalState = useGlobalState();
  const listen = globalState[LISTEN_STATE_CHANGE];
  const change = globalState[CHANGE_STATE];
  const time = listen(LISTENER_TEST1_CLICK);

  const onClickHandler = (e: any) => {
    e.preventDefault();
    change(LISTENER_TEST1_CLICK, Date.now());
  };

  return (
    <div>
      <a href="#" onClick={onClickHandler}>
        Test1
        <br />
        {time}
      </a>
    </div>
  );
};
