import {
  IGlobalState,
  IGetGlobalState,
  LISTEN_STATE_CHANGE,
  REMOVE_LISTENER,
  getGlobalState,
} from '@hooks/global-state';
import { useEffect } from '@lib/xreact';
import { LISTENER_TEST1_CLICK } from './constants';


const useGlobalState: IGetGlobalState = getGlobalState();

export const Test2 = () => {
  const globalState: IGlobalState = useGlobalState();
  const listen = globalState[LISTEN_STATE_CHANGE];
  const remove = globalState[REMOVE_LISTENER];
  const time = listen(LISTENER_TEST1_CLICK);

  useEffect(() => {
    return () => {
      remove();
    };
  }, []);

  return (
    <div>
      Test2
      <br />
      {time}
    </div>
  );
};
