import { IHtmlPageData } from '@services/get-page-data/interface';

export interface IPageProps {
  pageData: IHtmlPageData;
}
