import {
  Fragment,
  FunctionComponent,
  h,
  render,
  VNode as FunctionComponentElement,
} from 'preact';
import 'preact/debug';

export type ReactFCE<P> = FunctionComponentElement<P>;
export type ReactFC<P> = FunctionComponent<P>;

export { h, render, Fragment };
export { hydrate, createContext, createElement } from 'preact';

export { useState, useEffect, useContext } from 'preact/hooks';

/**
 * Unmount component at node
 * @param {Element} element
 */
export function unmountComponentAtNode(element: Element) {
  render(h(Fragment, null), element);
}
