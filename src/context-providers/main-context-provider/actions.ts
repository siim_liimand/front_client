import {
  IDynamicImportCallback,
  IDynamicImportCallbackData,
  IDynamicImportComponents,
} from '@hooks/dynamic-import/interfaces';
import { ReactFCE } from '@lib/xreact';
import { IDynamicImportCallbackResolve, IMainContext } from './interfaces';

export const createMainContext = (
  importCallbacks: IDynamicImportCallbackData[],
  importedComponents: IDynamicImportComponents,
): IMainContext => {
  const addImportCallback = (
    key: string,
    callback: IDynamicImportCallback,
    getComponent: IDynamicImportCallbackResolve,
  ) => {
    importCallbacks.push({
      callback,
      key,
    });
    callback((value: ReactFCE<any>): void => {
      getComponent(value);
    });
  };

  const getImportedComponent = (key: string): ReactFCE<any> | null => {
    return importedComponents[key] || null;
  };

  const setImportedComponent = (key: string, component: ReactFCE<any>): void => {
    const index = importCallbacks.map((o) => o.key).indexOf(key);
    if (index > -1) {
      importCallbacks.splice(index, 1);
    }
    importedComponents[key] = component;
  };

  return [addImportCallback, getImportedComponent, setImportedComponent];
};
