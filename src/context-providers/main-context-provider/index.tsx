import { createContext, ReactFC, ReactFCE } from '@lib/xreact';
import { createMainContext } from './actions';
import { IMainContext, IMainContextProviderProps } from './interfaces';

export const MainContext = createContext(createMainContext([], {}));

/**
 * Main context provider
 * @param {IMainContextProviderProps} props
 * @returns Element
 */
export const MainContextProvider: ReactFC<IMainContextProviderProps> = ({
  c,
  icd,
  ic,
}): ReactFCE<{ value: any }> => {
  const context: IMainContext = createMainContext(icd, ic);

  return <MainContext.Provider value={context}>{c}</MainContext.Provider>;
};
