import {
  IDynamicImportCallback,
  IDynamicImportCallbackData,
  IDynamicImportComponent,
  IDynamicImportComponents,
} from '@hooks/dynamic-import/interfaces';
import { ReactFCE } from '@lib/xreact';
import { JSX } from 'preact/jsx-runtime';

export interface IMainContextProviderProps {
  c: ReactFCE<JSX.Element>;
  icd: IDynamicImportCallbackData[];
  ic: IDynamicImportComponents;
}

export type IDynamicImportCallbackResolve = (component: IDynamicImportComponent) => void;

export type IAddImportCallback = (
  key: string,
  callback: IDynamicImportCallback,
  getComponent: IDynamicImportCallbackResolve,
) => void;

export type IGetImportedComponent = (key: string) => IDynamicImportComponent | null;
export type ISetImportedComponent = (key: string, component: IDynamicImportComponent) => void;

export type IMainContext = [IAddImportCallback, IGetImportedComponent, ISetImportedComponent];
