import { App } from '@app/index';
import { IAppProps } from '@app/interfaces';
import { MainContextProvider } from '@context/main-context-provider';
import {
  IDynamicImportCallbackData,
  IDynamicImportComponent,
} from '@hooks/dynamic-import/interfaces';
import { hydrate, ReactFCE, render, unmountComponentAtNode } from '@lib/xreact';

const loaderElId = 'r';
const importCallbacks: IDynamicImportCallbackData[] = [];
const importedComponents: {
  [key: string]: IDynamicImportComponent;
} = {};











// import { addSiteRoute } from './config/routes/site-routes';
// import { addComponent } from './router';
// import { IGetPage } from './router/interfaces';

// // constants/pages
// const PAGE_HOME = 'h';

// // constants/sites
// const SITE_DEFAULT = 'd';

// addComponent(PAGE_HOME, (getPage: IGetPage): void => {
//   import(/* webpackChunkName: "home" */ '@pages/home').then((module) => {
//     getPage(module.default);
//   });
// });

// addSiteRoute(SITE_DEFAULT, '127.0.0.1');

// createClient();










/**
 * Create client
 */
export function createClient(): void {
  render(createAppElement(), createLoaderContainer());
  waitAndAskIfImportsAreLoaded();
}

/**
 * Are imports loaded
 * @returns {boolean}
 */
function areImportsLoaded(): boolean {
  return importCallbacks.length === 0;
}

/**
 * Wait and ask if imports are loaded
 */
function waitAndAskIfImportsAreLoaded(): void {
  setTimeout(() => {
    if (areImportsLoaded()) {
      replaceRootElement();
    } else {
      waitAndAskIfImportsAreLoaded();
    }
  }, 50);
}

/**
 * Replace root element
 */
function replaceRootElement() {
  removeLoaderContainer();
  const rootElement: Element = document.getElementById('root') || document.createElement('<div>');
  hydrate(createAppElement(), rootElement);
}

/**
 * Create App element
 * @returns {ReactFCE<IAppProps>}
 */
function createAppElement(): ReactFCE<IAppProps> {
  const location = window.location;
  const hostName = location.hostname;
  const path = `${location.pathname}`;
  const query = new URLSearchParams(location.search);

  return (
    <MainContextProvider icd={importCallbacks} ic={importedComponents} c={<App h={hostName} p={path} q={query} />} />
  );
}

/**
 * Create loader container
 * @returns {Element}
 */
function createLoaderContainer(): Element {
  const loader = document.createElement('div');
  loader.style.display = 'none';
  loader.id = loaderElId;
  document.body.appendChild(loader);

  return loader;
}

/**
 * Remmove loader container
 */
function removeLoaderContainer(): void {
  const loaderElement = document.getElementById(loaderElId);
  if (loaderElement) {
    unmountComponentAtNode(loaderElement);
    loaderElement.innerHTML = '';
    loaderElement.remove();
  }
}
