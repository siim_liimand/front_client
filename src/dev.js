const webpack = require('webpack');
const webpackConfigClient = require('../webpack.config');

const compiler = webpack([
  {
    ...webpackConfigClient,
    mode: 'development',
    output: {
      ...webpackConfigClient.output,
      filename: '[name].js',
    },
  },
]);

compiler.hooks.watchRun.tap('Dev', (compiler) => {
  console.info(`Compiling ${compiler.name} ...`);
});

compiler.watch({}, (err, stats) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.info(stats?.toString('minimal'));
});
